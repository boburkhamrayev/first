from django.db import models
from django.shortcuts import reverse
from django.utils import timezone


class Category(models.Model):
    title = models.CharField("Category",max_length=
    255,db_index=True,null=True)
    slug = models.SlugField('slug',unique=True)

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.title 

    def get_absolute_url(self):
        return reverse('category_detail_url', kwargs={'slug':self.slug})  



class Post(models.Model):
    title = models.CharField('Sarlavha', max_length=255, db_index=True)
    slug = models.SlugField('Manzil', unique=True)
    description = models.CharField('Malumot', max_length=255, db_index=True)
    image = models.ImageField('Rasm', db_index=True)
    date = models.DateTimeField('Sana', default=timezone.now)
    category = models.ForeignKey(Category,related_name='categories',null=True,on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Post"
        verbose_name_plural = "Postlar"

    def __str__(self):
        return self.title 

    def get_absolute_url(self):
        return reverse('news_detail_url', kwargs={'slug':self.slug})       


