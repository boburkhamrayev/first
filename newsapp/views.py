from django.shortcuts import render, redirect

from django.http import HttpResponse

from .models import Post,Category

from django.db.models import Q

from .forms import RegisterForm

# def index(request):
#     return HttpResponse("Hello, world!")

def index(request):
    post = Post.objects.order_by('date')[:3]
    categorye = Category.objects.all()
    return render(request, 'base.html', {'post':post,'categorye':categorye})


def news_detail(request, slug):
    news = Post.objects.get(slug__iexact = slug)
    return render(request, 'news_detail.html', context={'news':news})    

def search_result(request):
    query = request.GET.get('search')
    search_obj = Post.objects.filter(
        Q(title__icontains=query) | Q(description__icontains=query)
    )
    return render(request,'search.html',{'search_obj':search_obj,'query':query})
    

def category_detail(request,slug):
    category =Category.objects.get(slug__iexact = slug)
    posts = Post.objects.all()
    return render(request,'category_detail.html',{'category':category,'posts':posts})

def register(request):
    if request.method == 'Post':
        form = RegisterForm(request.Post)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = RegisterForm()
    return render(request, 'register.html', {'form':form})
                    