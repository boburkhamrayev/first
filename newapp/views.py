from django.shortcuts import render,redirect,reverse

from django.http import HttpResponse,HttpResponseRedirect,Http404

from .models import Post,Category,Comment
from django.db.models import Q

from .forms import RegisterForm






def index(request):
    post = Post.objects.order_by('date')[:3]
    categorye = Category.objects.all()
    popular = Post.objects.filter(popular__gte = 10)
    return render(request, 'base.html',{'post':post,'categorye':categorye,'popular':popular})


def news_detail(request, slug):
    news = Post.objects.get(slug__iexact = slug)
    news.popular += 1
    news.save()
    return render(request, 'news_detail.html', context={'news':news })



def search_result(request):
    query = request.GET.get('search')
    search_obj = Post.objects.filter(
        Q(title__icontains=query) |Q(description__icontains=query)
    )
    return render(request, 'search.html', {'search_obj':search_obj, 'query':query})




def category_detail(request,slug):
    category = Category.objects.get(slug__iexact=slug)
    posts = Post.objects.order_by('date') 
    return render(request,'category_detail.html',{'category':category,'posts':posts})


def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            #log the user in
            return redirect('index')
    else:
        form = RegisterForm()
    return render(request, 'register.html', {'form':form})




def leave_comment(request, slug):
    try:
        news = Post.objects.get(slug__iexact = slug)
    except:
        raise Http404("Article not found")
    if request.user.is_authenticated:
        user = request.user.username
        news.comments.create(author_name = user , comment_text = request.POST.get('comment_text'))
    else:
        news.comments.create(author_name = request.POST.get('name'), comment_text = request.POST.get('comment_text'))
        comment = Comment.objects.order_by('date')[:2]
    return HttpResponseRedirect(reverse('news_detail_url' , args = (news.slug,)))

